package com.horizzon.cruxido.AaNewDetails.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.horizzon.cruxido.R;

public class MobileNumberActivity extends AppCompatActivity {
    private LinearLayout bottom_sheet;
    private BottomSheetBehavior sheetBehavior;
    private EditText editTextMobile;
    Button btn_bottom_sheet, login_button;
    Toolbar toolbar;
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);

        bottom_sheet=findViewById(R.id.bottom_sheet);
      //  sheetBehavior = BottomSheetBehavior.from(bottom_sheet);


        DisplayMetrics screenMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(screenMetrics);
        int screenHeight = screenMetrics.heightPixels;
        int screenWidth = screenMetrics.widthPixels;
        bottom_sheet.getLayoutParams().height = (screenHeight / 2);

        editTextMobile = bottom_sheet.findViewById(R.id.editTextMobile);
        login_button = bottom_sheet.findViewById(R.id.buttonContinue);
        editTextMobile.requestFocus();
        editTextMobile.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager keyboard = (InputMethodManager) MobileNumberActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.hideSoftInputFromWindow(editTextMobile.getWindowToken(), 0);
                    login_button.setText("continue");
                    login_button.setTextColor(getResources().getColor(R.color.white));

                    //do here your stuff f
                    return true;
                }
                return false;
            }


        });
        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() >=10) {
                    login_button.setText("continue");
                    login_button.setTextColor(getResources().getColor(R.color.white));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        bottom_sheet.findViewById(R.id.buttonContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile = editTextMobile.getText().toString().trim();

                if (mobile.isEmpty() || mobile.length() < 10) {
                    editTextMobile.setError("Enter a valid mobile");
                    editTextMobile.requestFocus();
                    return;
                }

                Intent intent = new Intent(MobileNumberActivity.this, phone_verifyActivity.class);
                intent.putExtra("mobile", mobile);
                startActivity(intent);
            }
        });


       /* sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
//                        btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
//                        btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });*/
    }


   /* @Override
    public boolean dispatchTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (sheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottom_sheet.getGlobalVisibleRect(outRect);

                if(!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                editTextMobile.setText("");
            }
        }

        return super.dispatchTouchEvent(event);
    }
*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
      //  sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
}
