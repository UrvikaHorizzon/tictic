package com.horizzon.cruxido.AaNewDetails.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.util.Util;
import com.horizzon.cruxido.R;

public class RvItemDataAdapter extends RecyclerView.Adapter<RvItemDataAdapter.MyViewHolder> {

    Context context;

    public RvItemDataAdapter(Context ctx) {
        this.context = ctx;

    }


    @NonNull
    @Override
    public RvItemDataAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rv_viewpagerfragment_data, parent, false);

        return new RvItemDataAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RvItemDataAdapter.MyViewHolder holder, int position) {
        //   holder.playerview.start();
     //   holder.player.stop();
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        PlayerView playerview;
        SimpleExoPlayer player;
        DefaultTrackSelector track_selector;
        DefaultBandwidthMeter band_width_meter = new DefaultBandwidthMeter();

        MediaSource mediaSource;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            playerview = itemView.findViewById(R.id.playerview);
            player = ExoPlayerFactory.newSimpleInstance(context);

            // Bind the player to the view.
            playerview.setPlayer(player);

            // Produces DataSource instances through which media data is loaded.
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "yourApplicationName"));

            // This is the MediaSource representing the media to be played.
            MediaSource firstSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(RawResourceDataSource.buildRawResourceUri(R.raw.videoplayback));

            // Prepare the player with the source.
            player.prepare(firstSource);
          /*  MediaController mediaController= new MediaController(context);
            mediaController.setAnchorView(playerview);
            //playerview.setMediaController(mediaController);
            String path = "android.resource://" + getAdapterPosition() + "/" + R.raw.bee_on_flower_1;
           // playerview.setVideoURI(Uri.parse(path));
           // playerview.setVideoURI(R.raw.bee_on_flower_1);
            playerview.requestFocus();
          //  context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

            TrackSelection.Factory video_track_selection_factory = new AdaptiveTrackSelection.Factory(band_width_meter);
            track_selector = new DefaultTrackSelector(video_track_selection_factory);
            video_player = ExoPlayerFactory.newSimpleInstance(context, track_selector);

            playerview.setPlayer(video_player);
            video_player.setPlayWhenReady(true);

            DataSource.Factory data_source_factory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "Application Name"), new DefaultBandwidthMeter());
            Uri url = Uri.parse(path);
            mediaSource = new ExtractorMediaSource.Factory(data_source_factory).createMediaSource(url);
            video_player.prepare(mediaSource);*/
        }
    }
}
