package com.horizzon.cruxido.AaNewDetails.fregment;

import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.horizzon.cruxido.AaNewDetails.adapter.RvViewpagerAdapter;
import com.horizzon.cruxido.R;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ViewPagerDataFragment extends Fragment {

    RecyclerView recyclerViewPager;
    RecyclerView.LayoutManager mLayoutManager;
    boolean is_user_stop_video = false;
    int currentPage = 0;

    public ViewPagerDataFragment() {
        // Required empty public constructor
    }

    public static ViewPagerDataFragment newInstance() {
        ViewPagerDataFragment fragment = new ViewPagerDataFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view, container, false);
        recyclerViewPager = view.findViewById(R.id.recyclerViewPager);
        RvViewpagerAdapter mAdapter = new RvViewpagerAdapter(getApplicationContext(), new RvViewpagerAdapter.IMyViewHolderClicks() {
            @Override
            public void onItemClick() {
//                Intent intent =new Intent(getApplicationContext(), OnActivity.class);
//                startActivity(intent);
            }
        });
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewPager.setLayoutManager(mLayoutManager);
        recyclerViewPager.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPager.setAdapter(mAdapter);
       /* SnapHelper snapHelper =  new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewPager);*/
        recyclerViewPager.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //here we find the current item number
                final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                final int height = recyclerView.getHeight();
                int page_no = scrollOffset / height;

                if (page_no != currentPage) {
                    currentPage = page_no;
                    Release_Privious_Player();
                    Set_Player(currentPage);
                }
            }
        });
        return view;
    }


    public void Set_Player(final int currentPage) {


        String proxyUrl = "android.resource://" + currentPage + "/" + R.raw.videoplayback;
        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(1 * 1024, 1 * 1024, 500, 1024).createDefaultLoadControl();

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector, loadControl);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(),
                Util.getUserAgent(getApplicationContext(), getApplicationContext().getResources().getString(R.string.app_name)));

        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(proxyUrl));


        player.prepare(videoSource);

        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        //  player.addListener(this);


        View layout = mLayoutManager.findViewByPosition(currentPage);
        final PlayerView playerView = layout.findViewById(R.id.playerViewMainScreen);
        playerView.setPlayer(player);


        //player.setPlayWhenReady(is_visible_to_user);
        privious_player = player;
        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    super.onFling(e1, e2, velocityX, velocityY);
                    float deltaX = e1.getX() - e2.getX();
                    float deltaXAbs = Math.abs(deltaX);
                    // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
                    if ((deltaXAbs > 100) && (deltaXAbs < 1000)) {
                        if (deltaX > 0) {
                            //OpenProfile(item,true);
                        }
                    }
                    return true;
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if (!player.getPlayWhenReady()) {
                        is_user_stop_video = false;
                        privious_player.setPlayWhenReady(true);
                    } else {
                        is_user_stop_video = true;
                        privious_player.setPlayWhenReady(false);
                    }
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    //  Show_video_option(item);
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    if (!player.getPlayWhenReady()) {
                        is_user_stop_video = false;
                        privious_player.setPlayWhenReady(true);
                    }


                    /*if(Variables.sharedPreferences.getBoolean(Variables.islogin,false)) {
                      //  Show_heart_on_DoubleTap(item, mainlayout, e);
                        Like_Video(currentPage, item);
                    }else {
                        Toast.makeText(context, "Please Login into app", Toast.LENGTH_SHORT).show();
                    }*/
                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

        TextView desc_txt = layout.findViewById(R.id.desc_txt);
        HashTagHelper.Creator.create(getApplicationContext().getResources().getColor(R.color.maincolor), new HashTagHelper.OnHashTagClickListener() {
            @Override
            public void onHashTagClicked(String hashTag) {

                onPause();
                // OpenHashtag(hashTag);

            }
        }).handle(desc_txt);


        LinearLayout soundimage = (LinearLayout) layout.findViewById(R.id.sound_image_layout);
        Animation sound_animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.d_clockwise_rotation);
        soundimage.startAnimation(sound_animation);

        /*if(Variables.sharedPreferences.getBoolean(Variables.islogin,false))
            Functions.Call_Api_For_update_view(getParent(),item.video_id);*/


       /* swipe_count++;
        if(swipe_count>6){
            Show_add();
            swipe_count=0;
        }



        Call_Api_For_Singlevideos(currentPage);*/

    }

     boolean is_visible_to_user;
     @Override
     public void setUserVisibleHint(boolean isVisibleToUser) {
         super.setUserVisibleHint(isVisibleToUser);
         is_visible_to_user=isVisibleToUser;

         if(privious_player!=null && (isVisibleToUser && !is_user_stop_video)){
             privious_player.setPlayWhenReady(true);
         }else if(privious_player!=null && !isVisibleToUser){
             privious_player.setPlayWhenReady(false);
         }
     }
    SimpleExoPlayer privious_player;
    public void Release_Privious_Player() {
        if (privious_player != null) {
           // privious_player.removeListener(getApplicationContext());
            privious_player.release();
        }
    }
}