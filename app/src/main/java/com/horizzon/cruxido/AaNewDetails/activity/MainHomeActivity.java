package com.horizzon.cruxido.AaNewDetails.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.horizzon.cruxido.R;
import com.horizzon.cruxido.AaNewDetails.adapter.ViewPagerAdapter;

public class MainHomeActivity extends AppCompatActivity {

    TabLayout tablayout;
    ViewPager viewPager;
    int no_of_weeks=6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);
        viewPager = findViewById(R.id.viewPager);
        tablayout = findViewById(R.id.tablayout);

        for (int i = 0; i < no_of_weeks; i++) {
            tablayout.addTab(tablayout.newTab().setText("Course " + String.valueOf(i + 1)));
        }

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(),tablayout.getTabCount());
        viewPager.setAdapter(adapter);
        tablayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.app_color));
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}