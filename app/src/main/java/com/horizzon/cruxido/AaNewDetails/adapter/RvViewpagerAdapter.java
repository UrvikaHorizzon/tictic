package com.horizzon.cruxido.AaNewDetails.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ui.PlayerView;
import com.horizzon.cruxido.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class RvViewpagerAdapter extends RecyclerView.Adapter<RvViewpagerAdapter.MyViewHolder> {

    Context context;
    IMyViewHolderClicks iMyViewHolderClicks;

    public RvViewpagerAdapter(Context ctx, IMyViewHolderClicks listener) {
        this.context = ctx;
        this.iMyViewHolderClicks = listener;
    }


    @NonNull
    @Override
    public RvViewpagerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_insidedata, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RvViewpagerAdapter.MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout;
        CircleImageView ImgUser;
        TextView txtUserName, txtFollow, txtDuration;
        PlayerView playerViewMainScreen;
        Button btnComment, btnShare, btnLike;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ImgUser = itemView.findViewById(R.id.ImgUser);
            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtFollow = itemView.findViewById(R.id.txtFollow);
            txtDuration = itemView.findViewById(R.id.txtDuration);
            playerViewMainScreen = itemView.findViewById(R.id.playerViewMainScreen);
            btnComment = itemView.findViewById(R.id.btnComment);
            btnLike = itemView.findViewById(R.id.btnLike);
            btnShare = itemView.findViewById(R.id.btnShare);

            /*relativeLayout = itemView.findViewById(R.id.mainlayout);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iMyViewHolderClicks.onItemClick();
                }
            });*/
        }
    }

    public static interface IMyViewHolderClicks {
        public void onItemClick();

        /*
         *  public void onLikeClick();
         *public void onCommentClick();
         * public void onShareClick();
         * */
    }

}
